﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n,v,s,rem=0,position=0;
            int i=0;

            Console.Write("Введите кол-во жильцов: ");
            n = int.Parse(Console.ReadLine());

            while(i<n)
            {
            start:
                Console.Write($"Введите возраст жильца №{i}: ");
                v = int.Parse(Console.ReadLine());
            
                Console.Write($"Введите пол жильца 1-м 0-ж №{i} : ");
                s = int.Parse(Console.ReadLine());
         
                 if(s>=2 || v>100)
                {
                    goto start;
                }
                else if (s == 1 && v > rem)
                {
                    rem = v;
                    position = i;
                }

                i++;
            }

            if (rem > 0)
            {
                Console.Write($"самый старый находится под номером {position}");
            }
            else
            {
                Console.Write("Мужчин нет ");
            }

            Console.ReadKey();
        }
    }
}
